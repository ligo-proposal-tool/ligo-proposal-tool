from __future__ import absolute_import
from __future__ import division
"""
Generate sample sky maps for a LIGO/Virgo observing scenario using BAYESTAR
"""


import argparse
from lalinference.bayestar import command


def get_parser():
    parser = command.ArgumentParser()
    parser.add_argument('input', type=argparse.FileType('rb'), default='-',
                        metavar='INPUT.hdf5', help='input HDF5 file')
    return parser


def main():
    parser = get_parser()
    args = parser.parse_args()

    #
    # Late imports
    #

    import astropy.coordinates
    import astropy.table
    import h5py
    import lal
    import lalsimulation
    import lpt.bayestar
    import lpt.config
    import lpt.fisher
    import lpt.job
    import logging
    import numpy as np
    from astropy.utils.console import ProgressBar

    logging.basicConfig(level=logging.INFO)
    log = logging.getLogger('LPT')

    #
    # Open input and output files
    #

    log.info('reading from %s', args.input.name)
    signal_models = astropy.table.Table.read(
        args.input.name, path='lpt/signal_models')
    ellipses = astropy.table.Table.read(
        args.input.name, path='lpt/ellipses')
    with h5py.File(args.input.name, mode='r') as f:
        config_text = f['lpt'].attrs['config']
    config = lpt.config.load(config_text)

    run_names = config['runs'].keys()
    log.info('runs: %s', ' '.join(run_names))

    psd_names = sorted(set(detector['psd'] for run in config['runs'].values() for detector in run['detectors'].values()))
    log.info('PSDs: %s', ' '.join(psd_names))

    masses = sorted(set((inj['mass1'], inj['mass2']) for inj in config['injections'].values()))
    log.info('masses: %s', ' '.join(str(_) for _ in masses))

    detector_names = sorted(set(detector for run in config['runs'].values() for detector in run['detectors'].keys()))
    log.info('detectors: %s', ' '.join(detector_names))

    log.info('looking up detector geometry')
    detectors = {detector_name: lalsimulation.DetectorPrefixToLALDetector(detector_name) for detector_name in detector_names}
    detectors = {detector_name: (detector.response, detector.location) for detector_name, detector in detectors.items()}

    fishers = {(krow['psd'], (krow['mass1'], krow['mass2'])): row for krow, row in zip(signal_models, signal_models['r1', 'w1', 'w2', 'sample_rate', 'acor'])}

    #
    # Look up Fisher matrix params
    #

    log.info('submitting BAYESTAR jobs to condor')
    for i, ellipse in enumerate(ProgressBar(ellipses)):
        detector_names, psd_names = zip(*((key, value['psd']) for key, value in
            config['runs'][ellipse['run']]['detectors'].items()))
        responses, locations = (np.copy(arr) for arr in zip(*(detectors[key] for key in detector_names)))
        r1s, w1s, w2s, sample_rates, acors = (np.copy(arr) for arr in zip(*(
            fishers[(psd_name, (ellipse['mass1'], ellipse['mass2']))]
            for psd_name in psd_names)))
        sample_rate = sample_rates[0]
        assert all(sample_rate == _ for _ in sample_rates)

        complex_snrs, _, _, _ = lpt.fisher.fisher_ellipse(ellipse['lon'], ellipse['lat'], ellipse['polarization'], ellipse['inclination'], ellipse['distance'], responses, locations, r1s, w1s, w2s)
        n = np.asarray(astropy.coordinates.spherical_to_cartesian(1.0, ellipse['lat'], ellipse['lon']))
        weights = w2s - np.square(w1s)
        toas = -np.dot(locations / lal.C_SI, n)
        locations -= np.average(locations, weights=weights, axis=0)
        toas -= np.average(toas, weights=weights)
        max_abs_t = np.max(
            np.sqrt(np.sum(np.square(locations / lal.C_SI), axis=1))) + 0.005
        max_n = int(np.ceil(max_abs_t * sample_rate))
        acors = acors[:, :max_n]
        acors = np.hstack((np.conj(acors[:, :0:-1]), acors))
        snr_series = acors * np.reshape(complex_snrs, (-1, 1))
        snr_series = snr_series.astype('complex64')

        max_horizon = max(r1s)
        min_distance = 0
        max_distance = max(r1s) / 4

        r1s /= max_horizon
        min_distance /= max_horizon
        max_distance /= max_horizon

        lpt.job.submit(lpt.bayestar.sky_map, i, max_horizon, (min_distance,
            max_distance, 2, 0, sample_rate, toas, snr_series, responses,
            locations, r1s))
