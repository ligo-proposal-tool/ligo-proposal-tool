"""Make a plot of the localization (Fisher ellipse and Bayestar posterior) of
each simulated system."""

import os
import argparse
import matplotlib
matplotlib.use('agg')


def get_parser():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('input', type=argparse.FileType('rb'),
                        metavar='INPUT.hdf5',
                        help='Input HDF5 file containing the full simulation')
    parser.add_argument('i', type=int, help='event ID')
    parser.add_argument('--level', type=float, default=90,
                        help='Credible percentile [default: %(default)s]')
    parser.add_argument('--output', type=str, default='.',
                        help='Base path where output plots will be saved [default: %(default)s]')
    return parser


def main():
    parser = get_parser()
    args = parser.parse_args()

    #
    # Late imports
    #

    from astropy.table import Table, join
    import logging
    from matplotlib import pyplot as plt
    import healpy as hp
    import numpy as np
    from reproject import reproject_from_healpix
    from .. import plot
    from shapely import geos
    from lalinference.bayestar import postprocess

    from astropy.io.fits import Header
    from astropy.wcs import WCS
    from astropy.visualization.wcsaxes.frame import RectangularFrame

    logging.basicConfig(level=logging.INFO)
    log = logging.getLogger('LPT')

    #
    # Open input and output files
    #

    log.info('reading from %s', args.input.name)
    table = join(
        Table.read(args.input.name, path='lpt/coincs'),
        Table.read(args.input.name, path='lpt/fisher_ellipses'))

    row = table[table['coinc_id'] == args.i][0]
    run = row['run']
    sim_id = row['coinc_id']

    header = Header(dict(
        NAXIS=2,
        NAXIS1=360, NAXIS2=180,
        CRPIX1=180, CRPIX2=90,
        CRVAL1=np.rad2deg(row['lon']), CRVAL2=np.rad2deg(row['lat']),
        CDELT1=2*np.sqrt(2)/np.pi,
        CDELT2=2*np.sqrt(2)/np.pi,
        CTYPE1='RA---TAN',
        CTYPE2='DEC--TAN',
        RADESYS='ICRS'))
    wcs = WCS(header)

    fig = plt.figure()
    ax = plt.axes([0.05, 0.2, 0.9, 0.8], projection=wcs,
                  frame_class=RectangularFrame, aspect=1)
    world_transform = ax.get_transform('world')
    ax.set_xlim(0, header['NAXIS1'])
    ax.set_ylim(0, header['NAXIS2'])
    fig.suptitle('Sim %d (%s) - %s network' %
                 (sim_id, row['template_id'], run))
    ax.grid(linestyle='-', color='black', alpha=0.25)

    # load and plot bayestar posterior
    bayestar_map_path = os.path.join(
            os.path.dirname(args.input.name),
            '%d.fits' % sim_id)
    bayestar_map, _ = hp.read_map(bayestar_map_path, h=True,
                                  verbose=False)
    cls = postprocess.find_greedy_credible_levels(bayestar_map)
    cls_repr, _ = reproject_from_healpix((cls, 'icrs'), header)
    ax.contour(cls_repr, levels=[args.level/100.],
               colors='#00ff00', lw=2, label='Bayestar')

    # draw Fisher ellipse
    try:
        ax.add_patch(plot.DatelineClippedCovariancePatch(
            row['lon'], row['lat'], row['pa'], row['a'], row['b'],
            level=args.level, facecolor='none', edgecolor='#ff0000',
            transform=world_transform, label='Fisher'))
    except geos.TopologicalError:
        log.exception('FIXME: skipping bad geometry')

    # true location
    ax.plot(np.rad2deg(row['lon']), np.rad2deg(row['lat']), 'xb',
            transform=world_transform, label='Simulation')

    # show all-sky map too
    ax = plt.axes([.65, .01, .3, .3], projection='lpt allsky')
    world_transform = ax.get_transform('world')
    ax.grid(linestyle='-', color='black', lw=0.5, alpha=0.25)

    cls_repr, _ = reproject_from_healpix((cls, 'icrs'), ax.header)
    ax.contour(cls_repr, levels=[args.level/100.],
               colors='#00ff00', label='Bayestar')

    try:
        ax.add_patch(plot.DatelineClippedCovariancePatch(
            row['lon'], row['lat'], row['pa'], row['a'], row['b'],
            level=args.level, facecolor='none', edgecolor='#ff0000',
            lw=0.5, transform=world_transform, label='Fisher'))
    except geos.TopologicalError:
        log.exception('FIXME: skipping bad geometry')

    # true location
    ax.plot(np.rad2deg(row['lon']), np.rad2deg(row['lat']), 'xb',
            transform=world_transform, label='Simulation')

    output_base = os.path.join(args.output, run)
    if not os.path.exists(output_base):
        os.mkdir(output_base)
    fig.savefig(os.path.join(output_base, '%.4d.png' % sim_id),
                dpi=200)
    plt.close(fig)
