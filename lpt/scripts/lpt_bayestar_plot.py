from __future__ import absolute_import
from __future__ import division
"""
Draw sky localization ellipse plots
"""


import argparse
import matplotlib
matplotlib.use('pdf')
from lalinference.bayestar import command


def get_parser():
    parser = command.ArgumentParser()
    parser.add_argument('input', type=argparse.FileType('rb'),
                        metavar='INPUT.hdf5', help='output HDF5 file')
    parser.add_argument('output', type=argparse.FileType('wb'),
                        metavar='OUTPUT.pdf', help='output PDF file')
    parser.add_argument('--level', type=float, default=90,
                        help='Credible percentile [default: %(default)s]')
    parser.add_argument('--draw-distance', default=False, action='store_true',
                        help='Draw distance heatmap')
    return parser


def main():
    parser = get_parser()
    args = parser.parse_args()

    #
    # Late imports
    #

    from astropy.io.fits import Header
    from astropy.table import Column, Table
    from astropy.wcs import WCS
    from astropy.utils.console import ProgressBar
    import collections
    import logging
    from matplotlib import pyplot as plt
    from matplotlib.collections import EllipseCollection
    import numpy as np
    from lalinference.io import fits
    from lalinference.bayestar import postprocess
    from lalinference.plot.backend_pdf_vcs_friendly import PdfPages
    from reproject import reproject_from_healpix
    from astropy.visualization.wcsaxes.frame import EllipticalFrame

    logging.basicConfig(level=logging.INFO)
    log = logging.getLogger('LPT')

    #
    # Open input and output files
    #

    log.info('reading from %s', args.input.name)
    table = Table.read(args.input.name, path='lpt/ellipses')
    table.add_column(Column(np.arange(len(table)), name='id'))

    header = Header(dict(
        NAXIS=2,
        NAXIS1=360, NAXIS2=180,
        CRPIX1=180, CRPIX2=90,
        CRVAL1=0, CRVAL2=0,
        CDELT1=2*np.sqrt(2)/np.pi,
        CDELT2=2*np.sqrt(2)/np.pi,
        CTYPE1='RA---AIT',
        CTYPE2='DEC--AIT',
        RADESYS='ICRS'))
    wcs = WCS(header)

    # Scale factor for given credible level
    scale = np.sqrt(-2 * np.log(1 - 0.01 * args.level))

    with PdfPages(args.output.name) as pdf:
        for run in collections.OrderedDict.fromkeys(table['run']):
            group = table[table['run'] == run]
            fig = plt.figure()
            ax = plt.axes(projection=wcs, frame_class=EllipticalFrame, aspect=1)
            world_transform = ax.get_transform('world')
            ax.set_xlim(0, header['NAXIS1'])
            ax.set_ylim(0, header['NAXIS2'])
            ax.set_title(group[0]['run'])
            ax.grid()

            # Plot heat map of distance
            if args.draw_distance:
                import healpy as hp
                import lal
                import lalsimulation
                npix = len(group)
                nside = hp.npix2nside(npix)
                distance = np.repeat(np.nan, npix)
                ipix = hp.ang2pix(nside, 0.5 * np.pi - group['lat'], group['lon'])
                distance[ipix] = group['distance']
                distance, _ = reproject_from_healpix((distance, 'icrs'), header)
                im = ax.imshow(distance, origin='lower', cmap='GnBu',
                               vmin=0,
                               vmax=table['distance'].max())
                cbar = plt.colorbar(im, orientation='horizontal')
                cbar.set_label('Distance (Mpc)')

            for row in ProgressBar(group):
                fitsfilename = '{}.fits.gz'.format(row['id'])
                prob, _ = fits.read_sky_map(fitsfilename)
                cls = postprocess.find_greedy_credible_levels(prob)
                cls, _ = reproject_from_healpix((cls, 'icrs'), header)
                ax.contour(cls, levels=[0.9], colors='black')

            pdf.savefig(fig)
