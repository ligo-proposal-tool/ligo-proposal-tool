from __future__ import absolute_import
from __future__ import division
"""
Generate sample sky maps for a LIGO/Virgo observing scenario using BAYESTAR
"""


import argparse
from lalinference.bayestar import command
from functools import reduce


def get_parser():
    parser = command.ArgumentParser()
    parser.add_argument('input', type=argparse.FileType('rb'), default='-',
                        metavar='INPUT.hdf5', help='input HDF5 file')
    parser.add_argument('i', type=int, help='event ID')
    return parser


def main():
    parser = get_parser()
    args = parser.parse_args()

    import logging
    logging.basicConfig(level=logging.INFO)
    log = logging.getLogger('LPT')

    #
    # Late imports
    #

    import astropy.coordinates
    import astropy.table
    import lal
    import lalsimulation
    from lalinference.io.fits import write_sky_map
    from lalinference.bayestar._sky_map import toa_phoa_snr
    from lalinference.bayestar import _moc as moc
    from lalinference.bayestar import distance
    from .. import fisher
    import numpy as np

    #
    # Open input file
    #

    runs = astropy.table.Table.read(
        args.input.name, path='lpt/runs')
    signal_models = astropy.table.Table.read(
        args.input.name, path='lpt/signal_models')
    coincs = astropy.table.Table.read(
        args.input.name, path='lpt/coincs')
    triggers = astropy.table.Table.read(
        args.input.name, path='lpt/triggers')
    signal_models = astropy.table.Table.read(
        args.input.name, path='lpt/signal_models')

    coinc = coincs[coincs['coinc_id'] == args.i]
    table = reduce(astropy.table.join, [coinc, triggers, runs, signal_models])
    coinc = coinc[0]

    n = np.asarray(astropy.coordinates.spherical_to_cartesian(
        1.0, coinc['lat'], coinc['lon']))
    detectors = [
        lalsimulation.DetectorPrefixToLALDetector(detector)
        for detector in table['detector']]
    responses = np.asarray([detector.response for detector in detectors])
    locations = np.asarray([detector.location for detector in detectors])
    w1s = np.asarray(table['w1'])
    w2s = np.asarray(table['w2'])
    r1s = np.asarray(table['r1'])
    acors = np.asarray(table['acor'])
    sample_rate = table[0]['sample_rate']
    assert all(table['sample_rate'] == sample_rate)
    abs_snr = table['snr']
    snrs = np.asarray(table['snr'] * fisher.exp_i(table['phase']))

    weights = np.square(abs_snr) * (w2s - np.square(w1s))
    toas = -np.dot(locations / lal.C_SI, n)
    locations -= np.average(locations, weights=weights, axis=0)
    toas -= np.average(toas, weights=weights)
    max_abs_t = np.max(
        np.sqrt(np.sum(np.square(locations / lal.C_SI), axis=1))) + 0.005
    max_n = int(np.ceil(max_abs_t * sample_rate))
    acors = acors[:, :max_n]
    acors = np.hstack((np.conj(acors[:, :0:-1]), acors))
    snr_series = acors * np.reshape(snrs, (-1, 1))
    snr_series = snr_series.astype('complex64')

    max_horizon = max(r1s)
    min_distance = 0
    max_distance = max(r1s) / 4

    skymap, _, _ = toa_phoa_snr(
        min_distance, max_distance, 2, 0, 0, sample_rate, toas, snr_series,
        responses, locations, r1s)
    skymap = astropy.table.Table(skymap)

    # Convert distance moments to parameters
    distmean = skymap.columns.pop('DISTMEAN')
    diststd = skymap.columns.pop('DISTSTD')
    skymap['DISTMU'], skymap['DISTSIGMA'], skymap['DISTNORM'] = \
        distance.moments_to_parameters(distmean, diststd)

    # Add marginal distance moments
    good = np.isfinite(distmean) & np.isfinite(diststd)
    prob = (moc.uniq2pixarea(skymap['UNIQ']) * skymap['PROBDENSITY'])[good]
    distmean = distmean[good]
    diststd = diststd[good]
    rbar = (prob * distmean).sum()
    r2bar = (prob * (np.square(diststd) + np.square(distmean))).sum()
    skymap.meta['distmean'] = rbar
    skymap.meta['diststd'] = np.sqrt(r2bar - np.square(rbar))

    # Fill in metadata and return.
    skymap.meta['creator'] = 'BAYESTAR / LIGO Proposal Tool'
    skymap.meta['origin'] = 'LIGO/Virgo'
    skymap.meta['objid'] = args.i
    write_sky_map('{0}.fits'.format(args.i), skymap)
