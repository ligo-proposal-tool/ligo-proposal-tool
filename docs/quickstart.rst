Quick Start
===========

Generate plots for the `Observing Scenarios Document`_::

    $ curl -O https://git.ligo.org/ligo-proposal-tool/ligo-proposal-tool/raw/master/lpt/examples/observing_scenarios.yml
    $ lpt observing_scenarios.yml observing_scenarios.hdf5
    $ lpt-plot observing-scenarios.hdf5 observing-scenarios.pdf


.. _Observing Scenarios Document: http://www.livingreviews.org/lrr-2016-1
