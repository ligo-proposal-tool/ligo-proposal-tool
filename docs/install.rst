Installation
============

 1. Make sure that you have installed both `LALInference`_ from master recently.

 2. Install ligo-proposal-tool using this command::

        $ pip install git+https://git.ligo.org/ligo-proposal-tool/ligo-proposal-tool

See :doc:`quickstart` for instructions on getting started using LIGO Proposal
Tool.


.. _LALInference: https://versions.ligo.org/cgit/lalsuite/tree/lalinference
.. _PyCBC: https://github.com/ligo-cbc/pycbc
