Welcome to LIGO Proposal Tool's documentation!
==============================================

Exploratory analysis and automation of sky localization studies for
gravitational-wave detector networks.

.. toctree::
   :maxdepth: 1

   install
   quickstart
   cmd
   api
